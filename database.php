<?php
function dbConnect(){
    $db_type = 'mysql' ; //資料庫類型
    $db_host = 'localhost';//資料庫主機名
    $db_name = 'message_board';//資料庫名稱
    $db_user = 'root';//用戶名
    $db_password = '';//密碼
    $dbconnect = "mysql:host=".$db_host.";dbname=".$db_name;//建立字串讓程式碼更易讀和維護
    $db = new PDO($dbconnect,$db_user,$db_password);//完成了與MySQL資料庫的連線
    $db->query("SET NAMES UTF8");
    return $db;
}

function addMsg($userName, $newMsg)
{
    $db = dbConnect();//連線到資料庫
    $dateTime = new DateTime();
    $today = $dateTime->format('Y-m-d'); // 抓取現在時間 Y-m-d 表示抓取 (年-月-日)(20XX-XX-XX)
    $statement = $db->prepare("INSERT INTO `message` (`name`, `content`, `date`) VALUES (?,?,?)");
    //PDO::prepare -為 PDO::execute 方法準備待執行的語句
    $statement->execute([$userName, $newMsg, $today]);//當執行時將用真實數據取代，執行成功會 return True，失敗則是 False
    
}

function showMsg()
{
    $db = dbConnect();//連線到資料庫
    $statement = $db->prepare("SELECT * FROM `message`");
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS);
    return $result;
}

function delectMsg($msgId)
{
    $db = dbConnect();//連線到資料庫
    $statement = $db->prepare("DELETE FROM `message` WHERE `message`.`msg_id` = ?");
    $statement->execute([$msgId]);
}

if (isset($_POST['add_msg'])) {
    addMsg($_POST['user_name'], $_POST['new_msg']);
}

if (isset($_POST['delect_msg'])) {
    delectMsg($_POST['msg_id']);
}

//註冊
function addMember($account, $password, $username){
    $db = dbConnect();//連線到資料庫
    $statement = $db->prepare("INSERT INTO `member` (`account`, `password`, `username`) VALUES (?,?,?)");
    $statement->execute([$account, $password,$username]);
}

if (isset($_POST['register'])) {
    addMember($_POST['account'], $_POST['password'], $_POST['username']);
}

?>