<?php
 include './database.php';
 $message = isset($_POST['register']);
 //調用 database.php 的 dbConnect 的 function; 
 $db = dbConnect();
 //判斷是否有拿到表單
 if(isset($_POST['register'])) {
 $account = $_POST['account'];
 $password = $_POST['password'];
 $username = $_POST['username'];
 // 檢查帳號是否已被註冊

 $query = "SELECT * FROM member WHERE account=:account";
 //更改為 $db
 $stmt = $db->prepare($query);
 $stmt->execute(['account' => $account]);
 $count = $stmt->rowCount();
 if($count > 0) {
    $message = "該帳號已被註冊！";
    } else {
    // 將帳號和密碼插入資料表中
    addMember($account, $password,$username);
    $message = "已成功註冊";
    }
    }
   ?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title>Register</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="text-center">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Register</h3>
                    </div>
                    <div class="panel-body">
                        <form action="Login.php" method="post">
                            <input type="text" class="form-control" name="account" placeholder="Account" required><br>
                            <input type="password" class="form-control" name="password" placeholder="Password" required><br>
                            <input type="text" class="form-control" name="username" placeholder="Username" required><br>
                            <button type="submit" class="btn btn-warning" name="register">註冊</button>
                            <a href="login.php">已有帳號想登入</a><br><br>
                        </form>
                        <?php if($message != ''){ ?>
                            <div class="alert alert-primary"role="alert">
                            <?php echo $message; ?>
                            </div>
                            <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
