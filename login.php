<?php
    include './database.php';
    // 登入
    $message = '';
    if(isset($_POST['login'])) {
        $account = $_POST['account'];
        $password = $_POST['password'];

        if($stmt->rowCount() == 1) {
            $message = "登入成功！";
            // 儲存使用者 ID 和使用者名稱到 session 中
            session_start();
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $_SESSION['user_id'] = $row['id'];
            $_SESSION['username'] = $row['username'];
            // sleep(3);
		    header("Location:index.php");// 將網址導回首頁
        } else {
            $message = "帳號或密碼錯誤！";
        }
    }
?>

<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title>Login</title>

</head>

<body>
    <div class="container">
        <div class="row">
            <div class="text-center">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Login</h3>
                    </div>
                    <div class="panel-body">
                        <form action="login.php" method="post">
                            <input type="text" class="form-control" name="account" placeholder="Account" required><br>
                            <input type="password" class="form-control" name="password" placeholder="Password" required><br>
                            <button type="submit" class="btn btn-primary" name="login">登入</button><br><br>
                            <a href="./register.php" class="btn btn-warning">註冊</a>
                        </form>
                        <?php if($message != ''){ ?>
                            <div class="alert alert-primary" role="alert">
                                <?php echo $message; ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
