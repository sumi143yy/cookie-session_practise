<?php
    include("database.php");
    include 'header.php';//引入導覽列
    $msg = showMsg();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap-5.1.1-dist/css/bootstrap.min.css">
    <script src="bootstrap-5.1.1-dist/js/bootstrap.bundle.js"></script>
    <link rel="stylesheet" href="style.css">
    <title>留言板</title>
</head>


<body>
    <div class="container">
        <div class="list">
            <h1 class="list_title text-center mt-5 mb-3">留言列表</h1>
            <?php foreach ($msg as $key => $row) { ?>
                <div class="card mb-2">
                    <div class="card-body">
                        <h5 class="card-title">
                        <!-- //改成使用物件屬性的方式來存取資料 -->
                            <?php echo $row->name ?>
                        </h5>
                        <p class="card-text">
                            <?php echo $row->content ?>
                        </p>
                        <form id="msg<?php echo $row->msg_id ?>" class="d-inline" method="post">
                            <input type="hidden" name="msg_id" value="<?php echo $row->msg_id ?>">
                            <input type="submit" name="delect_msg" class="btn btn-danger" value="刪除"></input>
                        </form>
                        <span class="date_text">
                            <?php echo $row->date ?>
                        </span>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="add_line text-center mt-5">
            <a class="btn btn-success px-5 mb-3" id="add_expend" data-bs-toggle="collapse" href="#collapseExample"
                role="button" aria-expanded="false" aria-controls="collapseExample" onclick="disappearance()">新增留言</a>
            <div class="collapse" id="collapseExample">
                <div class="card card-body">
                    <form method="post">
                        <input class="d-block form-control mb-2" type="text" name="user_name" placeholder="留言者姓名"
                            required>
                        <textarea class="d-block form-control" name="new_msg" rows="10" placeholder="請輸入內容"
                            required></textarea>
                        <input class="btn btn-success px-5 mt-3" type="submit" value="確認新增" name="add_msg">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        function disappearance() {
            document.querySelector('#add_expend').style.display = "none";
            //classList.remove() 方法移除 collapseExample 區塊的 class="collapse" 屬性，這樣就可以讓表單區塊顯示在畫面上了。
            document.querySelector('#collapseExample').classList.remove('collapse');
        }
    </script>
</body>

</html>