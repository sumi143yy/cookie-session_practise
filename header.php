<html>

<head>

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>

<body>

  <body>
    <nav class="navbar bg-light">
      <div class="container-fluid">
        <a class="navbar-brand fw-bolder">留言板</a>
        <?php  echo $_login  ?>
        <div class="nav navbar-right">
          <span class="navbar-text p-1">
             <?php echo $_COOKIE['username']; ?>
          </span>
          <a href="logout.php"><button class="btn btn-sm btn-outline-secondary">登出</button></a>
        </div>
      </div>
    </nav>
  </body>

</html>
